[![alt text](https://i.imgur.com/EOsZGPu.png)](https://forum.gta.world/en/)
# GTAW Dealership Manager - Releases

- Application requires an account, in order to obtain one, please contact [Vash Baldeus](https://forum.gta.world/en/profile/333-vash-baldeus/).
- This repository is only used for release & auto-update of the program itself on end-client machine.

# Information about the program

- This program is used to monitor employees at player-owned second hand dealerships on GTA World English Roleplay server.
- Any information within is strictly for IC purposes use of owners and employees at dealerships on the server.